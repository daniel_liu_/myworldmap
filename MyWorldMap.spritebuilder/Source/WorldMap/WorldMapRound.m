//
//  WorldMapRound.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "WorldMapRound.h"

#import "MapRoundLoader.h"
#import "BuildingIconNode.h"

@implementation WorldMapRound
{
    CCNode      *_roundRootNode;
    CCSprite    *_loadingSprite;
    
    NSMutableArray  *_mapRoundIndexs;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isLoading = NO;
        self.currentRoundIndex = 0;
    }
    return self;
}

- (void)dealloc
{
    [_roundRootNode removeObserver:self forKeyPath:@"position"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _roundRootNode) {
        if ([keyPath isEqualToString:@"position"]) {
            CGPoint newPos = [[change objectForKey:NSKeyValueChangeNewKey] CGPointValue];
            self.currentRoundIndex = [self calMapRoundIndexByPos:newPos];
        }
    }
}

/**
 * 往后翻
 *   到[1]时加载[2][3]，删除[-1][-2]
 *   到[3]时加载[4][5]，删除[0][1]
 *   到[5]时加载[6][7]，删除[2][3]
 * 往前翻
 *   到[4]时加载[2][3]，删除[6][7]
 *   到[2]时加载[0][1]，删除[4][5]
 *   到[0]时加载[-1][-2]，删除[2][3]
 */
- (void)setCurrentRoundIndex:(int)newIndex
{
    if (newIndex != _currentRoundIndex)
    {
        //  往后翻
        if (newIndex > _currentRoundIndex && newIndex % 2 == 1)
        {
            for (int i = 1; i <= 2; i++) {
                [self loadMapRoundAtIndex:newIndex+i];
                [self recycleMapRoundAtIndex:newIndex-i-1];
            }
        }
        //  往前翻
        if (newIndex < _currentRoundIndex && newIndex % 2 == 0)
        {
            for (int i = 1; i <= 2; i++) {
                [self loadMapRoundAtIndex:newIndex-i];
                [self recycleMapRoundAtIndex:newIndex+i+1];
            }
        }
        
        _currentRoundIndex = newIndex;
    }
}

- (void)didLoadFromCCB
{
    //  init map round indexs
    _mapRoundIndexs = [[NSMutableArray alloc] initWithCapacity:self.maxRoundCount];
    for (int i = 0; i < self.maxRoundCount; i++) {
        _mapRoundIndexs[i] = [NSNumber numberWithInteger:0];
    }
    
    //  add observer to roundRootNode's position
    [_roundRootNode addObserver:self forKeyPath:@"position" options:(NSKeyValueObservingOptionNew) context:NULL];
    
    //  load rounds
    for (int i = 0; i < self.minRoundCount; i++) {
        [self loadMapRoundAtIndex:i];
    }
}

- (void)mapRoundLoadFinished:(CCNode *)mapRoundNode
{
    NSString *mapRoundName = mapRoundNode.name;
    int idx = [[[mapRoundName componentsSeparatedByString:@"_"] lastObject] intValue];
    
    //  load animaitons
    NSString *animMapName = [NSString stringWithFormat:@"CCB/anims/map_anim_%d", idx];
    [MapRoundLoader loadMapAnimations:animMapName onNode:mapRoundNode pTarget:self];
    
    //  load buildings
    NSString *buildingMapName = [NSString stringWithFormat:@"CCB/buildings/map_building_%d.ccbi", idx];
    [MapRoundLoader loadMapBuildings:buildingMapName onNode:mapRoundNode pTarget:self];
    
    mapRoundNode.position = ccp(0, self.contentSize.height * idx);
    
    [self showLoading:NO];
}

- (void)mapAnimsLoadFinished:(CCNode *)animMapNode {
}

- (void)mapBuilingdsLoadFinished:(CCNode *)buildingMapNode
{
    BuildingIconNode *building01 = (BuildingIconNode *)[buildingMapNode getChildByName:@"building01" recursively:YES];
    if (building01) {
        [building01 setActive];
    }
}

- (CGPoint)validPosition:(CGPoint)pos
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    
    float y = pos.y;
    float maxHeight = -self.contentSize.height * self.maxRoundCount + viewSize.height;
    
    if (pos.y > 0) { y = 0; }
    if (pos.y < maxHeight) { y = maxHeight; }
    
    return ccp(0, y);
}

- (void)moveBy:(CGPoint)deltaPos
{
    if (self.isLoading) {
        return;
    }
    
    CGPoint newPos = ccpAdd(_roundRootNode.position, ccp(deltaPos.x, -deltaPos.y));
    _roundRootNode.position = [self validPosition:newPos];
}

- (void)moveTo:(CGPoint)newPos withAnimation:(BOOL)bAnim
{
    if (self.isLoading) {
        return;
    }
    
    CGPoint thePos = [self validPosition:newPos];
    if (bAnim) {
        [_roundRootNode stopAllActions];
        [_roundRootNode runAction:[CCActionMoveTo actionWithDuration:.35 position:thePos]];
    }else {
        _roundRootNode.position = thePos;
    }
}

- (int)calMapRoundIndexByPos:(CGPoint)pos
{
    float absY = fabsf(pos.y);
    return (absY / self.contentSize.height);
}

- (void)loadMapRoundAtIndex:(int)idx
{
    if (idx < 0 || idx >= _mapRoundIndexs.count) {
        CCLOG(@"out of bounds at: %d", idx);
        return;
    }
    
    if ([_mapRoundIndexs[idx] intValue] == 1) {
        CCLOG(@"no need to load map round at: %d", idx);
        return;
    }
    
    CCLOG(@" + load map round at: %d", idx);
    
    //  loading
    [self showLoading:YES];
    
    NSString *mapRoundName = [NSString stringWithFormat:@"CCB/mapround/map_round_%d", idx];
    [MapRoundLoader loadMapRound:mapRoundName onNode:_roundRootNode pTarget:self];
    _mapRoundIndexs[idx] = [NSNumber numberWithInteger:1];
}

- (void)recycleMapRoundAtIndex:(int)idx
{
    if (idx < 0 || idx >= _mapRoundIndexs.count) {
        CCLOG(@"out of bounds at: %d", idx);
        return;
    }
    
    if ([_mapRoundIndexs[idx] intValue] == 0) {
        CCLOG(@"no need to recycle map round at: %d", idx);
        return;
    }
    
    CCLOG(@" - recycle map round at: %d", idx);
    
    NSString *mapRoundName = [NSString stringWithFormat:@"map_round_%d", idx];
    [[_roundRootNode getChildByName:mapRoundName recursively:YES] removeFromParent];
    _mapRoundIndexs[idx] = [NSNumber numberWithInteger:0];
    
    [[CCDirector sharedDirector] purgeCachedData];
}

- (void)showLoading:(BOOL)isShow
{
    if (self.isLoading == isShow) {
        return;
    }
    
    self.isLoading = isShow;
    [_loadingSprite stopAllActions];
    
    if (isShow) {
        CCActionSequence *loadingAction = [CCActionSequence actionOne:[CCActionFadeOut actionWithDuration:.5]
                                                                  two:[CCActionFadeIn actionWithDuration:.5]];
        [_loadingSprite runAction:[CCActionRepeatForever actionWithAction:loadingAction]];
    }else {
        [_loadingSprite runAction:[CCActionFadeOut actionWithDuration:.5]];
    }
}

@end
