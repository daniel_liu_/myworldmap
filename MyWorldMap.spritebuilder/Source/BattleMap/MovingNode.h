//
//  MovingNode.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "cocos2d.h"

@protocol DraggableNode <NSObject>
@required
- (void)draggingWithDistance:(float)distance angle:(float)angle;
- (void)releaseWithDistance:(float)distance angle:(float)angle;
@end

@interface MovingNode : CCNode<DraggableNode>

@property (nonatomic, assign) float linearDamping;      // 线性阻尼
@property (nonatomic, assign) float angularDamping;     // 旋转阻尼
@property (nonatomic, assign) float power;              // 冲击力，决定impluse的大小

@property (nonatomic, assign) BOOL ignoreTouch;         // 过滤touch

- (void)gotoPos:(CGPoint)newPos;
- (void)gotoPos:(CGPoint)newPos multiple:(float)ratio;

@end

