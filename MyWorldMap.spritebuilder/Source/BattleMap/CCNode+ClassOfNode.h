//
//  CCNode+ClassOfNode.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 8/10/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface CCNode (ClassOfNode)

- (NSArray*)findChildrenOfClass:(Class)class;

@end
