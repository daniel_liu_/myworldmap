//
//  MapRoundLoader.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "MapRoundLoader.h"

@implementation MapRoundLoader

+ (NSData *)getCCBDataWithFilePath:(NSString *)path
{
    if (![path hasSuffix:@".ccbi"]) path = [path stringByAppendingString:@".ccbi"];
    NSString *fullpath = [[CCFileUtils sharedFileUtils] fullPathForFilename:path];
    return [NSData dataWithContentsOfFile:fullpath];
}

- (void)mapAnimsLoadFinished:(NSString *)animMapName{
}

+ (void)loadMapAnimations:(NSString *)animMapName onNode:(CCNode *)parentNode pTarget:(id)pSender
{
    [[self class] loadMapAnimations:animMapName onNode:parentNode pTarget:pSender z:1];
}

+ (void)loadMapAnimations:(NSString *)animMapName onNode:(CCNode *)parentNode pTarget:(id)pSender z:(int)zOrder
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *animMapData = [[self class] getCCBDataWithFilePath:animMapName];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (animMapData) {
                CCNode *animMapNode = [[CCBReader reader] loadWithData:animMapData owner:nil];
                [parentNode addChild:animMapNode z:zOrder];
                if (pSender && [pSender respondsToSelector:@selector(mapAnimsLoadFinished:)]) {
                    [pSender performSelector:@selector(mapAnimsLoadFinished:) withObject:animMapNode];
                }
            }
        });
    });
}

- (void)mapBuilingdsLoadFinished:(NSString *)buildingMapName {
}

+ (void)loadMapBuildings:(NSString *)buildingMapName onNode:(CCNode *)parentNode pTarget:(id)pSender
{
    [[self class] loadMapBuildings:buildingMapName onNode:parentNode pTarget:pSender z:1];
}

+ (void)loadMapBuildings:(NSString *)buildingMapName onNode:(CCNode *)parentNode pTarget:(id)pSender z:(int)zOrder
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *buildingData = [[self class] getCCBDataWithFilePath:buildingMapName];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (buildingData) {
                CCNode *buildingMapNode = [[CCBReader reader] loadWithData:buildingData owner:nil];
                [parentNode addChild:buildingMapNode z:zOrder];
                if (pSender && [pSender respondsToSelector:@selector(mapBuilingdsLoadFinished:)]) {
                    [pSender performSelector:@selector(mapBuilingdsLoadFinished:) withObject:buildingMapNode];
                }
            }
        });
    });
}

- (void)mapRoundLoadFinished:(CCNode *)mapRoundNode {
}

+ (void)loadMapRound:(NSString *)mapRoundName onNode:(CCNode *)parentNode pTarget:(id)pSender
{
    [[self class] loadMapRound:mapRoundName onNode:parentNode pTarget:pSender z:1];
}

+ (void)loadMapRound:(NSString *)mapRoundName onNode:(CCNode *)parentNode pTarget:(id)pSender z:(int)zOrder
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *mapRoundData = [[self class] getCCBDataWithFilePath:mapRoundName];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (mapRoundData) {
                CCNode *mapRoundNode = [[CCBReader reader] loadWithData:mapRoundData owner:nil];
                [parentNode addChild:mapRoundNode z:zOrder];
                if (pSender && [pSender respondsToSelector:@selector(mapRoundLoadFinished:)]) {
                    [pSender performSelector:@selector(mapRoundLoadFinished:) withObject:mapRoundNode];
                }
            }
        });
    });
}

@end

