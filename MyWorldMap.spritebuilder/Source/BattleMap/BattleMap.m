//
//  BattleMap.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 8/9/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "BattleMap.h"
#import "CCNode+ClassOfNode.h"

#define kScaleRate      0.002
#define kScaleMax       1
#define kScaleMin       0.5

@implementation BattleMap

- (void)didLoadFromCCB {
    //  no gravity
    self.gravity = CGPointZero;
    self.collisionDelegate = self;
    
    //  init dead areas
    _deadAreaList = [self getChildByName:@"DeadAreas" recursively:NO].children;
    for (CCNode *node_ in _deadAreaList) {
        node_.physicsBody.sensor = YES;
    }
    
    //  init players
    _playerList = [self findChildrenOfClass:[MovingNode class]];
    for (MovingNode *node_ in _playerList) {
        node_.physicsBody.collisionType = @"player";
    }
    
    //  touches
    self.userInteractionEnabled = YES;
    
    //  debug
//    self.debugDraw = YES;
}

- (CGPoint)locationForTouch:(UITouch *)touch {
    return [touch locationInView:touch.view];
}

- (void)updatePosition:(CGPoint)newPos {
    CGSize vsize = [[CCDirector sharedDirector] viewSize];
    float maxX = self.contentSize.width * self.scale - vsize.width;
    float maxY = self.contentSize.height * self.scale - vsize.height;
    if (newPos.x > 0) { newPos.x = 0; }
    if (newPos.x < -maxX) { newPos.x = -maxX; }
    if (newPos.y > 0) { newPos.y = 0; }
    if (newPos.y < -maxY) { newPos.y = -maxY; }
    self.position = newPos;
}

- (void)makePlayersIgnoreTouch:(BOOL)isIgnore {
    for (MovingNode* node_ in _playerList) {
        [node_ setIgnoreTouch:isIgnore];
    }
}

- (void)setScale:(float)scale {
    [super setScale:scale];
    [self updatePosition:self.position];
}

#pragma mark - Touches

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    //  reset all status
    _lastScaleDistance = 0.0f;
    _lastScaleDelta = 0.0f;
    [self makePlayersIgnoreTouch:NO];
    
    NSArray* touches = event.allTouches.allObjects;
    if (touches.count > 1) {
        [self makePlayersIgnoreTouch:YES];
        CGPoint loc1 = [self locationForTouch:[touches objectAtIndex:0]];
        CGPoint loc2 = [self locationForTouch:[touches objectAtIndex:1]];
        _lastScaleDistance = ccpDistance(loc1, loc2);
    }
}

- (void)touchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    NSArray* touches = event.allTouches.allObjects;
    if (touches.count > 1) {
        CGPoint loc1 = [self locationForTouch:[touches objectAtIndex:0]];
        CGPoint loc2 = [self locationForTouch:[touches objectAtIndex:1]];
        float scaleDistance = ccpDistance(loc1, loc2);
        float scaleDelta = scaleDistance - _lastScaleDistance;
        if (_lastScaleDelta > scaleDelta) {
            float newScale = self.scale - (_lastScaleDelta - scaleDelta) * kScaleRate;
            if (newScale <= kScaleMin) { newScale = kScaleMin; }
            self.scale = newScale;
        }else {
            float newScale = self.scale + (scaleDelta - _lastScaleDelta) * kScaleRate;
            if (newScale >= kScaleMax) { newScale = kScaleMax; }
            self.scale = newScale;
        }
        _lastScaleDelta = scaleDelta;
    }else {
        CGPoint lastPos = [touch previousLocationInView:touch.view];
        CGPoint cntPos = [touch locationInView:touch.view];
        CGPoint deltaPos = ccpSub(cntPos, lastPos);
        CGPoint newPos = ccpAdd(self.position, ccp(deltaPos.x, -deltaPos.y));
        [self updatePosition:newPos];
    }
}

- (void)touchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    //  a delay disignore
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self makePlayersIgnoreTouch:NO];
    });
}

- (void)touchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    [self touchEnded:touch withEvent:event];
}

#pragma mark - CollisionDelegate

-(BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA death:(CCNode *)nodeB {
    //  make a dead anim action
    [nodeA stopAllActions];
    CCActionInterval *deadAction = [CCActionSpawn actions:[CCActionRotateBy actionWithDuration:.25 angle:270],
                                    [CCActionScaleTo actionWithDuration:.25 scale:.35], nil];
    [nodeA runAction:[CCActionSequence actions:deadAction, [CCActionRemove action], nil]];
    
    return YES;
}

-(BOOL)ccPhysicsCollisionPreSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA death:(CCNode *)nodeB {
    //  TODO:: find a way to stop the current player
//    nodeA.physicsBody.type = CCPhysicsBodyTypeStatic;
    
    return YES;
}

@end
