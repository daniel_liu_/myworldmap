//
//  WorldMapRound.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "cocos2d.h"

@interface WorldMapRound : CCNode

//  custom property from CCB's
@property (nonatomic, assign) int minRoundCount;
@property (nonatomic, assign) int maxRoundCount;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) int currentRoundIndex;

- (void)moveBy:(CGPoint)deltaPos;
- (void)moveTo:(CGPoint)newPos withAnimation:(BOOL)bAnim;
- (int)calMapRoundIndexByPos:(CGPoint)pos;
- (void)loadMapRoundAtIndex:(int)idx;
- (void)recycleMapRoundAtIndex:(int)idx;

- (void)showLoading:(BOOL)isShow;

@end
