//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"

@implementation MainScene

- (void)goWorldmap
{
    CCTransition *transition = [CCTransition transitionFadeWithColor:[CCColor blackColor] duration:.5];
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"CCB/WorldMap"] withTransition:transition];
}

@end
