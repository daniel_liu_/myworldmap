//
//  CCNode+ClassOfNode.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 8/10/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode+ClassOfNode.h"

@implementation CCNode (ClassOfNode)

// Return all children of a specific class
- (NSArray*)findChildrenOfClass:(Class)class
{
    NSMutableArray* nodes = [NSMutableArray array];
    [self addChildrenOfClass:class toArray:nodes forNode:self];
    return nodes;
}

- (void)addChildrenOfClass:(Class)class toArray:(NSMutableArray*)array forNode:(CCNode*)node
{
    for (CCNode* child in node.children)
    {
        if ([child isKindOfClass:class])
        {
            [array addObject:child];
        }
        
        [self addChildrenOfClass:class toArray:array forNode:child];
    }
}

@end
