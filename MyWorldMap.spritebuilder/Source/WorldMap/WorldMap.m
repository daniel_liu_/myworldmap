//
//  WorldMap.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "WorldMap.h"
#import "WorldMapRound.h"

@implementation WorldMap
{
    WorldMapRound *_mapRound;
    
    UIGestureRecognizer *_gesture;
}

- (void)dealloc
{
    [[[CCDirector sharedDirector] view] removeGestureRecognizer:_gesture];
}

- (void)didLoadFromCCB
{
    //  add gesture
    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureCallback:)];
    [[[CCDirector sharedDirector] view] addGestureRecognizer:panGes];
    _gesture = panGes;
}

- (void)panGestureCallback:(UIPanGestureRecognizer *)gesture
{
    CGPoint translationPos = [gesture translationInView:gesture.view];
    [_mapRound moveBy:translationPos];
    [gesture setTranslation:CGPointZero inView:gesture.view];
}

- (void)goBackClicked
{
    CCTransition *transition = [CCTransition transitionFadeWithColor:[CCColor blackColor] duration:.5];
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"CCB/MainScene"] withTransition:transition];
}

@end
