//
//  MapIconNode.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "MapIconNode.h"

@implementation MapIconNode
{
    BOOL                _isActive;
    CCParticleSystem*   _particle;
}

- (void)setActive:(BOOL)bActive
{
    _isActive = bActive;
    
    CCAnimationManager *animMgr = self.userObject;
    [animMgr runAnimationsForSequenceNamed:bActive?@"active":@"inactive"];
}

- (void)activeParticle:(BOOL)bActive
{
    [_particle setVisible:bActive];
}

- (void)iconClicked
{
    CCLOG(@"icon clicked for type: %d & level id: %d", self.type, self.levelId);
    
    if (self.levelId %2 == 0) {
        [self setActive:!_isActive];
    }
}

@end
