//
//  MapRoundLoader.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "cocos2d.h"

@interface MapRoundLoader : NSObject

+ (NSData *)getCCBDataWithFilePath:(NSString *)path;

- (void)mapAnimsLoadFinished:(CCNode *)animMapNode;
+ (void)loadMapAnimations:(NSString *)animMapName onNode:(CCNode *)parentNode pTarget:(id)pSender;
+ (void)loadMapAnimations:(NSString *)animMapName onNode:(CCNode *)parentNode pTarget:(id)pSender z:(int)zOrder;

- (void)mapBuilingdsLoadFinished:(CCNode *)buildingMapNode;
+ (void)loadMapBuildings:(NSString *)buildingMapName onNode:(CCNode *)parentNode pTarget:(id)pSender;
+ (void)loadMapBuildings:(NSString *)buildingMapName onNode:(CCNode *)parentNode pTarget:(id)pSender z:(int)zOrder;

- (void)mapRoundLoadFinished:(CCNode *)mapRoundNode;
+ (void)loadMapRound:(NSString *)mapRoundName onNode:(CCNode *)parentNode pTarget:(id)pSender;
+ (void)loadMapRound:(NSString *)mapRoundName onNode:(CCNode *)parentNode pTarget:(id)pSender z:(int)zOrder;

@end
