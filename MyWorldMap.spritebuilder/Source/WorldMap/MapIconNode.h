//
//  MapIconNode.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "cocos2d.h"

@interface MapIconNode : CCNode

@property (nonatomic, assign) int type;
@property (nonatomic, assign) int levelId;

- (void)setActive:(BOOL)bActive;
- (void)activeParticle:(BOOL)bActive;

@end
