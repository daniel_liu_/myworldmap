//
//  MovingNode.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 6/23/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "MovingNode.h"

@implementation MovingNode
{
    CCSprite    *_arrowSpt;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        self.ignoreTouch = NO;
    }
    return self;
}

#pragma mark - 触摸事件

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
}

- (void)touchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (self.ignoreTouch) {
        return;
    }
    
    CGPoint location = [touch locationInNode:self.parent];
    
    float diffx = self.position.x - location.x;
    float diffy = self.position.y - location.y;
    
    float angle = CC_RADIANS_TO_DEGREES(atan2f(diffx, diffy));
    float distance = sqrtf(powf(diffx, 2) + powf(diffy, 2));
    
    //  protocol call
    [self draggingWithDistance:distance angle:angle];
}

- (void)touchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (self.ignoreTouch) {
        return;
    }
    
    CGPoint location = [touch locationInNode:self.parent];
    
    float diffx = self.position.x - location.x;
    float diffy = self.position.y - location.y;
    
    float angle = atan2(diffy, diffx);
    float distance = sqrt(pow (diffx, 2) + pow(diffy, 2));
    
    //  protocol call
    [self releaseWithDistance:distance angle:angle];
}

- (void)touchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
    [self touchEnded:touch withEvent:event];
}

#pragma mark - 阻尼

- (float)mag
{
    CGPoint v = self.physicsBody.velocity;
    return pow(v.x, 2) + pow(v.y, 2);
}

- (void)update:(CCTime)delta
{
    CCPhysicsBody *body = self.physicsBody;
    
    CGPoint v = body.velocity;
    float w = body.angularVelocity;
    
    v = ccpMult(v, clampf(1.0f - delta * self.linearDamping, 0.0f, 1.0f));
    w *= clampf(1.0f - delta * self.angularDamping, 0.0f, 1.0f);
    
    body.velocity = v;
    body.angularVelocity = w;
    
    //  stop condition
    if ([self mag] < 0.5f) {
        body.velocity = CGPointZero;
        body.angularVelocity = 0.0f;
    }
}

#pragma mark - 移动

- (void)gotoPos:(CGPoint)newPos
{
    [self gotoPos:newPos multiple:1.0];
}

- (void)gotoPos:(CGPoint)newPos multiple:(float)ratio
{
    CGPoint pt = ccpSub(newPos, self.position);
    CGPoint rt = ccpMult(pt, ratio);
    [self.physicsBody applyImpulse:rt];
}

#pragma mark - DraggableNode

- (void)draggingWithDistance:(float)distance angle:(float)angle
{
    self.rotation = angle;
    
    _arrowSpt.visible = YES;
    _arrowSpt.scaleY = 0.4 + MIN(distance * 0.002, 0.3);
    
    //  subclass implement
}

- (void)releaseWithDistance:(float)distance angle:(float)angle
{
    CGPoint impluse = CGPointMake(cosf(angle) * self.power * distance,
                                  sinf(angle) * self.power * distance);
    [self.physicsBody applyImpulse:impluse];
    
    _arrowSpt.visible = NO;
    _arrowSpt.scaleY = 0.4;
    
    //  subclass implement
}

@end

