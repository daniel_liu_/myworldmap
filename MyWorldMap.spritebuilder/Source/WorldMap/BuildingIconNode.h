//
//  BuildingIconNode.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 8/15/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface BuildingIconNode : CCNode

- (void)setActive;
- (void)setInactive;

@end
