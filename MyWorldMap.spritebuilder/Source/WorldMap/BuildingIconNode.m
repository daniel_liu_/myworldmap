//
//  BuildingIconNode.m
//  MyWorldMap
//
//  Created by LiuHuanMing on 8/15/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "BuildingIconNode.h"

@implementation BuildingIconNode

- (void)setActive
{
    CCAnimationManager *animMgr = self.userObject;
    [animMgr runAnimationsForSequenceNamed:@"active"];
}

- (void)setInactive
{
    CCAnimationManager *animMgr = self.userObject;
    [animMgr runAnimationsForSequenceNamed:@"inactive"];
}

@end
