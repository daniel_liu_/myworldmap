//
//  BattleMap.h
//  MyWorldMap
//
//  Created by LiuHuanMing on 8/9/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

#import "MovingNode.h"

@interface BattleMap : CCPhysicsNode<CCPhysicsCollisionDelegate> {
    //  data list
    NSArray* _deadAreaList;
    NSArray* _playerList;
    
    //  for map scale
    float _lastScaleDelta;
    float _lastScaleDistance;
}

@end
